# Terraria#

Kode awal dari Terraria yang sebelumnya telah diedit.

### Untuk apa repository ini ? ###
* Mempermudah seseorang untuk mendapatkan Terraria secara gratis
* Membuat seseorang dapat mengedit Terraria dengan mudah
* Dan tentunya, tanpa game file.

## _Penting!_ ##
* Anda harus menginstall .NET Framework minimal v4.0.30319
* Anda harus menginstall Visual Studio 2010 atau yang lebih tinggi
* Anda harus menginstall Visual C++ 2010 atau lebih baru
* Minimal Windows XP
* CPU berkecepatan 1,6 GHz
* RAM minimal 2 GB
* Anda harus sudah menginstall Terraria sebelumnya
* Source Code ini memerlukan CSteamworks.dll